# pass2keepass
Convert pass ([passwordstore.org](https://www.passwordstore.org/)) to Keepass 2.x XML file.

# Usage
**Note:** You may need to adapt the script for yourself.

	bash ./pass2keepass.sh

# Links
A script for the opposite direction can be found here: [keepass2pass](https://github.com/ewoutp/keepass2pass).
