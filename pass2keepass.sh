#!/bin/bash


Accounts=$(cd ~/.password-store; find . -type f -name "*.gpg" | grep -vE ".jpg.|.dat|.pdf|.txt|.p12")

csvFile="./`basename "$0" .sh`.csv"
rm $csvFile
touch $csvFile
echo "title;user;password;notes" >>${csvFile}

for i in $Accounts;
do
  # Regex not necessary away
  i=$(echo "$i" | sed -e "s/\.\///")
  i=$(echo "$i" | sed -e "s/\.gpg//")

  origFile=`mktemp`
  echo "Exporting: $i"
  pass $i >$origFile
  Password=`head -1 $origFile`
  echo "	${i};${i};${Password};\"`cat ${origFile}`\"	" >>${csvFile}
  shred -u $origFile
done
